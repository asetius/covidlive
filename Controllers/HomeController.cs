using Microsoft.AspNetCore.Mvc;
using covidlive.Models;

namespace covidlive.Controllers
{
    public class HomeController : Controller
    {
        private readonly ICountryRepository _repository;

        public HomeController(ICountryRepository repository)
        {
            _repository = repository;
        }

        public IActionResult Index()
        {
            var list = _repository.Countries;
            return View(list);
        }
    }
}