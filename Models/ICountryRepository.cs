namespace covidlive.Models
{
    public interface ICountryRepository
    {
        IQueryable<Country> Countries { get; }
    }
}