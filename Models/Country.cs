namespace covidlive.Models
{
    public class Country
    {
        public int Id { get; set; }
        public string? CountryName { get; set; }
        public int TotalCases { get; set; }
        public int TotalRecovered { get; set; }
    }
}